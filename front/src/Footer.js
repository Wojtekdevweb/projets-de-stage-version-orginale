import React, { Component } from 'react'
import LogoOhMyLot from './Photos/bulle-oymylot-white.png'
import './Footer.scss'
import Widget from './services/FacebookWidget'

class Footer extends Component {
  render () {
    return (
      <footer>
        <div className='footer'>
          <a className='footerHref' href="http://lot.fr"><i className="fas fa-home faa-pulse animated-hover"></i>ACCEUIL</a>
          <a className='footerHref' href="http://lot.fr/nous-contacter"><i className="fas fa-envelope-open faa-shake animated-hover"></i>NOUS CONTACTER</a>
          <a className='facebookLogo footerHref' href="https://www.facebook.com/lot.departement"><i className="fab fa-facebook-square faa-tada animated-hover"></i>REJOIGNEZ-NOUS SUR FACEBOOK !</a>
          <a href="http://oh-my-lot.fr/"><img className='logoOhMyLot faa-tada animated-hover footerHref' src={LogoOhMyLot} alt='logo'/> Oh My Lot</a>
          <a href='http://lot.fr/mentions-l-gales' className='footerHref Mentions'>Mentions Légales</a>
        </div>
        <div className='widgetSection'>
          <Widget/>
        </div>
      </footer>
    )
  }
}
export default Footer
