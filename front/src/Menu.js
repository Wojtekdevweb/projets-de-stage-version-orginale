import React from 'react'
import './Menu.scss'
import LogoOhMyLot from './Photos/bulle-oymylot-white.png'

function Menu () {
  return (
    <div className='menu'>
      <div className='largeMenuTop'>
        <nav>
          <div className='leftSideMenu'>
            <a className='img-fluid' href="http://lot.fr"><i className="fas fa-home faa-pulse animated-hover"></i>ACCEUIL</a>
            <a className='img-fluid' href="http://lot.fr/nous-contacter"><i className="fas fa-envelope-open faa-shake animated-hover"></i>NOUS CONTACTER</a>
          </div>
          <div className='rightSideMenu'>
            <a className='facebookLogo' href="https://www.facebook.com/lot.departement"><i className="fab fa-facebook-square faa-tada animated-hover"></i></a>
            <a href="http://oh-my-lot.fr/"><img className='logoOhMyLot faa-tada animated-hover' src={LogoOhMyLot} alt='logo'/></a>
          </div>
        </nav>
      </div>
      <div className='smallMenuTop'>
        <nav className="navbar navbar-light light-blue lighten-4">
          <button className="navbar-toggler toggler-example text-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
            <span className="dark-blue-text"><i className="fas fa-bars fa-1x"></i></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent1">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link text-white homeicon" href="http://lot.fr"><i className="fas fa-home"></i>ACCEUIL</a>
                <a className="nav-link text-white mailicon" href="http://lot.fr/nous-contacter"><i className="fas fa-envelope-open"></i>NOUS CONTACTER</a>
              </li>
              <li className="nav-item">
                <a className="nav-link text-primary" href="https://www.facebook.com/lot.departement"><i className="fab fa-facebook-square facebookLogo"></i> REJOIGNEZ-NOUS SUR FACEBOOK !</a>
              </li>
              <li className="nav-item">
                <a className="nav-link ohMyLot" href="http://oh-my-lot.fr/"><img className='logoOhMyLot faa-tada' src={LogoOhMyLot} alt='logo'/> OH MY LOT</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  )
}

export default Menu
