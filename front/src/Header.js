import React from 'react'
import LogoLeft from './Photos/LotLogo.png'
import LogoRight from './Photos/poleEmplois.png'
import './Header.scss'

function Header () {
  return (
    <header>
      <div className='Header'>
        <div className='divLogoLeft'>
          <a href="http://lot.fr">
            <img className='logoLeft' src={LogoLeft} alt='logo'/>
            <p>Des Lotois au service des Lotois</p>
          </a>
        </div>
        <h1>Offres d'emploi</h1>
        <div className='divLogoRight'>
          <a href="https://www.pole-emploi.fr/accueil/">
            <img className='logoRight' src={LogoRight} alt='logo'/>
          </a>
        </div>
      </div>
    </header>
  )
}
export default Header
