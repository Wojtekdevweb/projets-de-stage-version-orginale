import React from 'react'
import L from 'leaflet'
import * as esri from 'esri-leaflet'
import './leafletMarkerCluster/leaflet.markercluster'
import '../services/leafletMarkerCluster/MarkerCluster.css'
import '../services/leafletMarkerCluster/MarkerCluster.Default.css'

let lgCommunes = L.layerGroup()
let commune = ''
let departement = ''
let region = ''
let lotLayer
let markers = L.markerClusterGroup()
let lgDepartements = L.layerGroup()
let lgRegions = L.layerGroup()
let marker = L.layerGroup()

// style de dessin des surfaces
let contourStyle = {
  getStyle: function () {
    let Style = {
      'color': '#6c9724',
      'weight': 2,
      'opacity': 2
    }
    return Style
  },
}
class LeafletMap extends React.Component {
  // no scroll Card
  noScrollFunction () {
    this.map.scrollWheelZoom.disable()
    this.map.on('click', () => { this.map.scrollWheelZoom.enable() })
    this.map.on('mouseout', () => { this.map.scrollWheelZoom.disable() })
  }
  componentDidMount () {
    this.map = L.map('map', {
      center: [44.6379358, 1.6760691],
      zoom: 9,
      type: 'satellite',
      layers: [
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }),
      ]
    })
    this.getLotContour()
  }
  getContourRegion () {
    this.map.removeLayer(lotLayer)
    lgRegions.clearLayers() // supprime le contenu du layer/removes the contents of the layer
    lgDepartements.clearLayers()
    lgCommunes.clearLayers()
    fetch(`https://public.opendatasoft.com/api/records/1.0/search/?dataset=contours-geographiques-des-regions-2019&rows=100&facet=region&facet=insee_reg&refine.insee_reg=${ this.props.region }&format=geojson`)
      .then(res => res.json())
      .then(json => {
        // add contour to map
        let geoJsonDataDep = json.features[0].properties.geo_shape
        region = L.geoJSON(geoJsonDataDep, contourStyle.getStyle()).addTo(lgRegions)
        this.map.fitBounds(region.getBounds())
        this.map.addLayer(lgRegions)
        // add markers to map
        markers.clearLayers() // supprime le markers du layer/removes the markers of the layer
        this.props.dataApi.forEach(function (el) {
          if (el.lieuTravail.latitude !== undefined && el.lieuTravail.longitude !== undefined) {
            let urlOffre = `<a href="#null" onclick="window.open('${ el.origineOffre.urlOrigine } ', '', 'width=800,height=800')">${ el.origineOffre.urlOrigine }</a>`
            markers.addLayer(L.marker([el.lieuTravail.latitude, el.lieuTravail.longitude]).bindPopup(`${ el.lieuTravail.libelle }<br> ${ el.appellationlibelle }<br> ${ urlOffre }`))
          }
        })
        this.map.addLayer(markers)
        this.noScrollFunction()
      })
  }
  getContourDepartement () {
    this.map.removeLayer(lotLayer)
    lgDepartements.clearLayers() // supprime le contenu du layer/removes the contents of the layer
    lgCommunes.clearLayers()
    lgRegions.clearLayers()
    fetch(`https://public.opendatasoft.com/api/records/1.0/search/?dataset=contours-geographiques-des-departements-2019&rows=100&facet=insee_dep&refine.insee_dep=${ this.props.departement }&format=geojson`)
      .then(res => res.json())
      .then(json => {
        // add contour to map
        let geoJsonDataDep = json.features[0].properties.geo_shape
        departement = L.geoJSON(geoJsonDataDep, contourStyle.getStyle()).addTo(lgDepartements)
        this.map.fitBounds(departement.getBounds())
        this.map.addLayer(lgDepartements)
        // add markers to map
        markers.clearLayers() // supprime le markers du layer/remove the markers from the layer
        this.props.dataApi.forEach(function (el) {
          if (el.lieuTravail.latitude !== undefined && el.lieuTravail.longitude !== undefined) {
            let urlOffre = `<a href="#null" onclick="window.open('${ el.origineOffre.urlOrigine } ', '', 'width=800,height=800')">${ el.origineOffre.urlOrigine }</a>`
            markers.addLayer(L.marker([el.lieuTravail.latitude, el.lieuTravail.longitude]).bindPopup(`${ el.lieuTravail.libelle }<br> ${ el.appellationlibelle }<br> ${ urlOffre }`))
          }
        })
        this.map.addLayer(markers)
        this.noScrollFunction()
      })
  }
  getContourCommune () {
    this.map.removeLayer(lotLayer)
    lgCommunes.clearLayers() // supprime le contenu du layer/remove the contents of the layer
    lgRegions.clearLayers()
    lgDepartements.clearLayers()
    fetch(`https://public.opendatasoft.com/api/records/1.0/search/?dataset=contours-geographiques-des-communes-et-arrondissements-municipaux-2019&rows=100&facet=insee_com&refine.insee_com=${ this.props.codeInsee }&format=geojson`)
      .then(res => res.json())
      .then(json => {
        // add contour to map
        let geoJsonData = json.features[0].properties.geo_shape
        commune = L.geoJSON(geoJsonData, contourStyle.getStyle()).addTo(lgCommunes)
        this.map.fitBounds(commune.getBounds())
        this.map.addLayer(lgCommunes)
        // add markers to map
        markers.clearLayers() // supprime le markers du layer/remove the marker from the layer
        this.props.dataApi.forEach(function (el) {
          if (el.lieuTravail.latitude !== undefined && el.lieuTravail.longitude !== undefined) {
            let urlOffre = `<a href="#null" onclick="window.open('${ el.origineOffre.urlOrigine } ', '', 'width=800,height=800')">${ el.origineOffre.urlOrigine }</a>`
            markers.addLayer(L.marker([el.lieuTravail.latitude, el.lieuTravail.longitude]).bindPopup(`${ el.lieuTravail.libelle }<br> ${ el.appellationlibelle }<br> ${ urlOffre }`))
          }
        })
        this.map.addLayer(marker)
        this.noScrollFunction()
      })
  }
  getAllFranceMarkers () {
    this.map.removeLayer(lotLayer)
    markers.clearLayers() // supprime le markers du layer/remove the markers from the layer
    this.props.dataApi.forEach(function (el) {
      if (el.lieuTravail.latitude !== undefined && el.lieuTravail.longitude !== undefined) {
        let urlOffre = `<a href="#null" onclick="window.open('${ el.origineOffre.urlOrigine } ', '', 'width=800,height=800')">${ el.origineOffre.urlOrigine }</a>`
        markers.addLayer(L.marker([el.lieuTravail.latitude, el.lieuTravail.longitude]).bindPopup(`${ el.lieuTravail.libelle }<br> ${ el.appellationlibelle }<br> ${ urlOffre }`))
      }
    })
    this.map.addLayer(markers)
    this.map.setZoom(5)
    this.noScrollFunction()
  }
  getLotContour () {
    lotLayer = esri.featureLayer({
      url: 'https://api-sig.lot.fr/services/GP_Territoiresadm/MapServer/6',
      style: function () {
        return {
          color: '#6c9724',
          weight: 2
        }
      }
    }).addTo(this.map)
  }
  componentDidUpdate () {
    // console.log('this.componentDidUpdate')
    if (this.props.codeInsee !== '') {
      this.getContourCommune()
    }
    if (this.props.departement) {
      this.getContourDepartement()
    }
    if (this.props.region) {
      this.getContourRegion()
    }
    if (this.props.region === '' && this.props.departement === '' && this.props.codeInsee === '') {
      this.getAllFranceMarkers()
    }
  }
  render () {
    return (
      <div id='map'></div>
    )
  }
}
export default LeafletMap
