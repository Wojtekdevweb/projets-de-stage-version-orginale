
function fetchToken () {
  return fetch('http://localhost:8000/getTokenApi.js')
    .then(res => res.json())
    .then(data => data)
    .catch(err => err)
}
function fetchInsee (commune) {
  return fetch(`https://geo.api.gouv.fr/communes?nom=${ commune }&fields=departement&boost=population`)
    .then(res => res.json())
    .then(data => data)
    .catch(err => err)
}
function getAllDepartements () {
  return fetch(`https://geo.api.gouv.fr/departements`)
    .then(res => res.json())
    .then(data => data)
    .catch(err => err)
}
function getAllRegions () {
  return fetch(`https://geo.api.gouv.fr/regions`)
    .then(res => res.json())
    .then(data => data)
    .catch(err => err)
}
export { fetchToken, fetchInsee, getAllDepartements, getAllRegions }
