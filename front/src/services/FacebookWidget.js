import React from 'react'

function Widget () {
  return (
    <iframe
      title="Iframe PoleEmplois"
      src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fpoleemploi&tabs=timeline&width=400&height=450&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId"
      width="390"
      height="450"
      scrolling="no"
      frameBorder="0"
      allow="encrypted-media">
    </iframe>
  )
}
export default Widget
