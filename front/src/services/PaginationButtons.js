import React, { Component } from 'react'

class PaginationButtons extends Component {
  constructor () {
    super()
    this.state = {
      range: '1-150'
    }
  }
  onChangeRange = event => {
    this.setState({
      range: event.target.value
    })
    this.props.callbackFromParent(event.target.value)
  }
  render () {
    const buttonStyle1 = (this.state.range === '1-150') ? { backgroundColor: '#6c9724' } : {}
    const buttonStyle2 = (this.state.range === '151-300') ? { backgroundColor: '#6c9724' } : {}
    const buttonStyle3 = (this.state.range === '301-450') ? { backgroundColor: '#6c9724' } : {}
    const buttonStyle4 = (this.state.range === '451-600') ? { backgroundColor: '#6c9724' } : {}
    return (
      <div>
        <div className="btn-group mr-2" role="group" aria-label="First group" id='pagination'>
          <button type="button" style={buttonStyle1} className="btn btn-secondary btn-sm" value='1-150' checked={this.state.range === '1-150'} onClick={this.onChangeRange}>0-150</button>
          <button type="button" style={buttonStyle2} className="btn btn-secondary btn-sm" value='151-300' checked={this.state.range === '151-300'} onClick={this.onChangeRange}>151-300</button>
          <button type="button" style={buttonStyle3} className="btn btn-secondary btn-sm" value='301-450' checked={this.state.range === '301-450'} onClick={this.onChangeRange}>301-450</button>
          <button type="button" style={buttonStyle4} className="btn btn-secondary btn-sm" value='451-600' checked={this.state.range === '451-600'} onClick={this.onChangeRange}>451-600</button>
        </div>
      </div>
    )
  }
}
export default PaginationButtons
