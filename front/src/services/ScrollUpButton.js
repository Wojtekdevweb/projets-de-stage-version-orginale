import React from 'react'
import ScrollUpButton from 'react-scroll-up-button'

export default class ScrollUpBtn extends React.Component {
  render () {
    return (
      <div>
        <ScrollUpButton
          AnimationDuration={800}
          style={{ backgroundColor: '#9bc23e' }}
        />
      </div>
    )
  }
}
