import React from 'react'
import './App.scss'
import Header from './Header'
import Menu from './Menu'
import Body from './Body'
import Footer from './Footer'

function App () {
  return (
    <div className="App">
      <Header />
      <Menu />
      <Body />
      <Footer />
    </div>
  )
}
export default App
