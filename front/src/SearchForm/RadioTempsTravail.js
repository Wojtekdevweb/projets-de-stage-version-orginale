import React, { Component } from 'react'
import './FormStyle.scss'

class RadioTempsTravail extends Component {
  constructor () {
    super()
    this.state = {
      tempsPlein: ''
    }
  }
  onChangeTempsPlein = event => {
    this.setState({
      tempsPlein: event.target.value
    })
    this.props.callbackFromParent(event.target.value)
  }
  render () {
    return (
      <div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            className="custom-control-input"
            id="defaultGroupExample1"
            name="groupOfDefaultRadios"
            value=''
            checked={this.state.tempsPlein === ''}
            onChange={this.onChangeTempsPlein}>
          </input>
          <label className="custom-control-label"
            htmlFor="defaultGroupExample1">non renseigné</label>
        </div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            className="custom-control-input"
            id="defaultGroupExample2"
            name="groupOfDefaultRadios"
            value= 'True'
            checked={this.state.tempsPlein === 'True'}
            onChange={this.onChangeTempsPlein}>
          </input>
          <label className="custom-control-label"
            htmlFor="defaultGroupExample2">temps plein</label>
        </div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            className="custom-control-input"
            id="defaultGroupExample3"
            name="groupOfDefaultRadios"
            value= 'False'
            checked={this.state.tempsPlein === 'False'}
            onChange={this.onChangeTempsPlein}>
          </input>
          <label className="custom-control-label"
            htmlFor="defaultGroupExample3">temps partiel</label>
        </div>
      </div>
    )
  }
}
export default RadioTempsTravail
