import React, { Component } from 'react'
import './FormStyle.scss'

class FormDistance extends Component {
  constructor () {
    super()
    this.state = {
      distance: ''
    }
  }
  onChangeDistance = event => {
    this.setState({
      distance: event.target.value
    })
    this.props.callbackFromParent(event.target.value)
  }
  render () {
    let myStyle = (this.props.departement || this.props.region) ? { opacity: 0.6 } : {}
    let optionStyle = (this.props.departement || this.props.region) ? { display: 'none' } : {}
    return (
      <div style={myStyle}>
        <label className="inputDistance"><i className="fas fa-map-marker-alt"></i> Distance</label>
        <select
          id="distanceInput"
          className="form-control"
          onChange = {this.onChangeDistance}
          value = {this.state.distance}>
          <option style={optionStyle} value="0">Lieu exact ...</option>
          <option style={optionStyle} value="5">5 km</option>
          <option style={optionStyle} value="10">10 km</option>
          <option style={optionStyle} value="20">20 km</option>
          <option style={optionStyle} value="30">30 km</option>
          <option style={optionStyle} value="40">40 km</option>
          <option style={optionStyle} value="50">50 km</option>
          <option style={optionStyle} value="60">60 km</option>
          <option style={optionStyle} value="70">70 km</option>
          <option style={optionStyle} value="80">80 km</option>
          <option style={optionStyle} value="90">90 km</option>
          <option style={optionStyle} value="100">100 km</option>

        </select>
      </div>
    )
  }
}
export default FormDistance
