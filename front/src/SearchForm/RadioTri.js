import React, { Component } from 'react'
import './FormStyle.scss'

class RadioTri extends Component {
  constructor () {
    super()
    this.state = {
      tri: ''
    }
  }
  onChangeTri = event => {
    this.setState({
      tri: event.target.value
    })
    this.props.callbackFromParent(event.target.value)
  }
  render () {
    return (
      <div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            id="customRadio1"
            name="customRadio"
            className="custom-control-input"
            value= '0'
            checked={this.state.tri === '0'}
            onChange={this.onChangeTri}>
          </input>
          <label
            className="custom-control-label"
            htmlFor="customRadio1">par pertinence</label>
        </div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            id="customRadio2"
            name="customRadio"
            className="custom-control-input"
            value= '1'
            checked={this.state.tri === '1'}
            onChange={this.onChangeTri}>
          </input>
          <label
            className="custom-control-label"
            htmlFor="customRadio2">par date
          </label>
        </div>
        <div className="custom-control custom-radio">
          <input
            type="radio"
            id="customRadio3"
            name="customRadio"
            className="custom-control-input"
            value= '2'
            checked={this.state.tri === '2'}
            onChange={this.onChangeTri}>
          </input>
          <label
            className="custom-control-label"
            htmlFor="customRadio3">par distance
          </label>
        </div>
      </div>

    )
  }
}
export default RadioTri
