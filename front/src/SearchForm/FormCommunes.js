import React, { Component } from 'react'
import './FormStyle.scss'
import AlgoliaPlaces from 'algolia-places-react'

class FormCommunes extends Component {
  constructor () {
    super()
    this.state = {
      commune: '',
    }
  }
  render () {
    let myStyle = (this.props.departement || this.props.region) ? { opacity: 0.6 } : {}
    return (
      <div style= {myStyle}>
        <div className="form-group">
          <label className="formGroupExampleInput2"><i className="fas fa-map-marker-alt"></i>  Commune</label>
          <div>
            <AlgoliaPlaces
              autoComplete="off"
              id="address-input"
              type="text"
              placeholder='Commune'
              options={{
                appId: 'plVH7AW3NAEA',
                apiKey: 'f6030118f42e6673931429b4e8eb6d05',
                language: 'fr',
                countries: ['fr'],
                type: 'city',
                // Other options from https://community.algolia.com/places/documentation.html#options
              }}
              // eslint-disable-next-line
            onChange={({ rawAnswer, query, suggestion, suggestonIndex }) => {  { this.setState({ commune: suggestion.name }); this.props.callbackFromParent(this.state.commune)} }}
            />
          </div>
        </div>
      </div>
    )
  }
}
export default FormCommunes
