import React, { Component } from 'react'
import './FormStyle.scss'

class FormMotsCles extends Component {
  constructor () {
    super()
    this.state = {
      motsCles: ''
    }
  }
    onChangeMotsCles = event => {
      this.setState({
        motsCles: event.target.value,
      })
      this.props.callbackFromParent(event.target.value)
    }
    render () {
      return (
        <div>
          <div className="form-group">
            <label className="formGroupExampleInput2"><i className="fas fa-briefcase"></i>  Métier, compétence</label>
            <input
              type="text"
              className="form-control"
              id="motsClesInput"
              onChange = {this.onChangeMotsCles}
              value = {this.state.motsCles}>
            </input>
          </div>
        </div>
      )
    }
}
export default FormMotsCles
