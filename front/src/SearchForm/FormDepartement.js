import React, { Component } from 'react'
import { getAllDepartements } from '../services/Apis'
import './FormStyle.scss'

class FormDepartement extends Component {
  constructor () {
    super()
    this.state = {
      departement: '',
      allDepartments: []
    }
  }
  componentDidMount () {
    getAllDepartements()
      .then(data => {
        let departmentsFromApi = data.map(department => { return { value: department, display: department } })
        this.setState({ allDepartments: [{ value: '', display: '' }].concat(departmentsFromApi) })
      }
      )
      .catch(error => {
        console.log(error)
      })
  }
    onChangeDepartement = event => {
      this.setState({
        departement: event.target.value
      })
      this.props.callbackFromParent(event.target.value)
    }
    render () {
      let myStyle = (this.props.commune || this.props.region) ? { opacity: 0.6 } : {}
      return (
        <div style={myStyle}>
          <label className="inputState"><i className="fas fa-map-marker-alt"></i> Departement</label>
          <select
            id="departementInput"
            className="form-control"
            value={this.state.departement}
            onChange={this.onChangeDepartement}>
            {(this.props.commune || this.props.region) || this.state.allDepartments.map((dep, i) =>
              <option key={i} value={dep.value.code}>{dep.display.code}-{dep.display.nom}</option>)}
          </select>
        </div>
      )
    }
}
export default FormDepartement
