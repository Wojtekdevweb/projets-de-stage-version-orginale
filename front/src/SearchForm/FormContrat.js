import React, { Component } from 'react'
import './FormStyle.scss'

class FormContrats extends Component {
  constructor () {
    super()
    this.state = {
      contrat: ''
    }
  }
    onChangeContrat = event => {
      this.setState({
        contrat: event.target.value
      })
      this.props.callbackFromParent(event.target.value)
    }
    render () {
      return (
        <div>
          <label className="inputState"><i className="fas fa-file-contract"></i>  Contrat</label>
          <select id="contratInput"
            className="form-control"
            onChange = {this.onChangeContrat}
            value = {this.state.contrat}>
            <option value="">Type de contrat ...</option>
            <option value="CCE">CCE</option>
            <option value="CDD">CDD</option>
            <option value="CDI">CDI</option>
            <option value="CDS">CDS</option>
            <option value="DDI">DDI</option>
            <option value="DIN">DIN</option>
            <option value="FRA">FRA</option>
            <option value="LIB">LIB</option>
            <option value="MIS">MIS</option>
            <option value="REP">REP</option>
            <option value="SAI">SAI</option>
            <option value="TTI">TTI</option>
          </select>
        </div>
      )
    }
}
export default FormContrats
