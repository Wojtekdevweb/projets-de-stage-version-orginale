import React, { Component } from 'react'
import { getAllRegions } from '../services/Apis'
import './FormStyle.scss'

class FormRegions extends Component {
  constructor () {
    super()
    this.state = {
      region: '',
      allRegions: []
    }
  }
  componentDidMount () {
    getAllRegions()
      .then(data => {
        let regionsFromApi = data.map(region => { return { value: region, display: region } })
        this.setState({ allRegions: [{ value: '', display: '' }].concat(regionsFromApi) })
      })
      .catch(error => {
        console.log(error)
      })
  }
    onChangeRegions = event => {
      this.setState({
        region: event.target.value
      })
      this.props.callbackFromParent(event.target.value)
    }
    componentWillUpdate () {
      if (this.props.reset) {
        getAllRegions()
          .then(data => {
            let regionsFromApi = data.map(region => { return { value: region, display: region } })
            this.setState({ allRegions: [{ value: '', display: '' }].concat(regionsFromApi) })
          })
          .catch(error => {
            console.log(error)
          })
      }
    }
    render () {
      if (this.props.reset === 'clear') {
        document.location.reload(true)
      }
      let myStyle = (this.props.departement || this.props.commune) ? { opacity: 0.6 } : {}
      return (
        <div style={myStyle}>
          <label className="inputState"><i className="fas fa-map-marker-alt"></i> Region</label>
          <select
            onReset={this.clearImputValue}
            id="regionInput"
            className="form-control"
            value={this.state.region}
            onChange={this.onChangeRegions}>
            {(this.props.commune || this.props.departement) || this.state.allRegions.map((reg, i) =>
              <option key={i} value={reg.value.code}>{reg.display.code}-{reg.display.nom}</option>)}
          </select>
        </div>
      )
    }
}
export default FormRegions
