import React, { Component } from 'react'
import Popup from 'reactjs-popup'
import './SingleOffert.scss'

class SingleOffert extends Component {
  constructor () {
    super()

    this.state = {
      visible: 5,
    }
  }
  loadMore () {
    this.setState(prev => {
      return { visible: prev.visible + 5 }
    })
  }
  render () {
    const popupStyle = {
      width: '100%',
      maxWidth: '60rem',
	    minWidth: '20rem',
      maxHeight: '90vh',
      overflowY: 'auto',
      marginTop: '30px',
      marginBottom: '30px',
      backgroundColor: '#ECECEC',
      margin: '20px 20px',
      textAlign: 'left',
      padding: '20px',
    }
    return (
      <section className="feed">
        <div className="tiles">
          {this.props.dataApi.slice(0, this.state.visible).map((el, index) => {
            return (
              <div className='Popup tile fade-in' key={index}>
                <Popup
                  trigger={
                    <button className="singleOffertContainer">
                      <div className="titreContainer">
                        <span className="offertNumber">{index + 1}</span>
                        <h1>{el.intitule}</h1>
                        <p className="extra Infos"><i className="fas fa-map-marker-alt"></i> {el.lieuTravail.libelle}</p>
                        <p className="extra Infos">{el.typeContrat} - {el.typeContratLibelle}</p>
                      </div>
                    </button>
                  }
                  modal
                  contentStyle={popupStyle}
                  closeOnDocumentClick
                >
                  {close => (
                    <div className="dataContainer" >
                      <button className="close" onClick={close}>
                       &times;
                      </button>
                      <div className="Offre">
                        <div className="titreContainer">
                          <h1>{el.intitule}</h1>
                          <p className="extra Titre"><i className="fas fa-map-marker-alt"></i> {el.lieuTravail.libelle}</p>
                          <p className="extra Infos">{el.typeContrat} - {el.typeContratLibelle}</p>
                        </div>
                        <div className='offertContainer'>
                          <p className="extra Titre">Actualisé le: {`${ new Date(el.dateActualisation).toLocaleDateString() }`} - offre n°{el.id}</p>
                          <p className="extra Infos">{el.description}</p>
                          <p className="extra Titre">PROFIL SOUHAITÉ :</p>
                          <p className="extra Titre">Expérience :</p>
                          <p className="extra Infos">{el.experienceLibelle}</p>
                          <p className="extra Titre">Savoirs et savoir-fair :</p>
                          <div className='competencyDiv'>
                            { el.competences[0] && <p className="extra Infos"> - {el.competences[0].libelle}</p>}
                            { el.competences[1] && <p className="extra Infos"> - {el.competences[1].libelle}</p>}
                            { el.competences[2] && <p className="extra Infos"> - {el.competences[2].libelle}</p>}
                            { el.competences[3] && <p className="extra Infos"> - {el.competences[3].libelle}</p>}
                            { el.competences[4] && <p className="extra Infos"> - {el.competences[4].libelle}</p>}
                            { el.competences[5] && <p className="extra Infos"> - {el.competences[5].libelle}</p>}
                            { el.competences[6] && <p className="extra Infos"> - {el.competences[6].libelle}</p>}
                            { el.competences[7] && <p className="extra Infos"> - {el.competences[7].libelle}</p>}
                            { el.competences[8] && <p className="extra Infos"> - {el.competences[8].libelle}</p>}
                            { el.competences[9] && <p className="extra Infos"> - {el.competences[9].libelle}</p>}
                            { el.competences[10] && <p className="extra Infos"> - {el.competences[10].libelle}</p>}
                            { el.competences[11] && <p className="extra Infos"> - {el.competences[11].libelle}</p>}
                          </div>
                          <p className="extra Titre">Savoir-être professionnels :</p>
                          { el.qualitesProfessionnelles && <p className="extra Infos"> - {el.qualitesProfessionnelles[0].libelle}</p>}
                          { el.qualitesProfessionnelles && el.qualitesProfessionnelles[1] && <p className="extra Infos"> - {el.qualitesProfessionnelles[1].libelle}</p>}
                          { el.qualitesProfessionnelles && el.qualitesProfessionnelles[2] && <p className="extra Infos"> - {el.qualitesProfessionnelles[2].libelle}</p>}
                          <p className="extra Titre">INFORMATIONS COMPLÉMENTAIRES :</p>
                          <p className="extra Infos">Secteur d'activité: {el.secteurActiviteLibelle}</p>
                          <p className="extra Titre">CONTRAT :</p>
                          <p className="extra Infos"><i className="fas fa-file-signature"></i> {el.typeContratLibelle}</p>
                          <p className="extra Infos"><i className="far fa-clock"></i> {el.dureeTravailLibelle}</p>
                          { el.salaire && <p className="extra Infos"><i className="fas fa-wallet"></i>{el.salaire.libelle}</p> }
                          <p className="extra Titre">ENTREPRISE :</p>
                          { el.entreprise && <p className="extra Infos">{el.entreprise.nom}</p> }
                          { el.entreprise && <p className="extra Infos">{el.entreprise.description}</p> }
                          { el.entreprise && <p className="extra Infos"><a className="UrlOffre" href={el.entreprise.url}>{el.entreprise.url}</a></p> }
                          <p className="extra Titre">ORIGINE OFFRE :</p>
                          <p className="extra Infos"><a className="UrlOffre" href={el.origineOffre.urlOrigine} target="_blank" rel="noopener noreferrer">{el.origineOffre.urlOrigine}</a></p>
                        </div>
                      </div>
                    </div>
                  )}
                </Popup>
              </div>
            )
          })}
        </div>
        {this.state.visible < this.props.dataApi.length &&
             <button onClick={this.loadMore.bind(this)} type="button" className="load-more btn btn-danger">AFFICHER PLUS D'OFFRES</button>
        }
      </section>
    )
  }
}
export default SingleOffert
