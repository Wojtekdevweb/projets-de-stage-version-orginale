import React, { Component } from 'react'
import './Body.scss'
import { fetchToken, fetchInsee } from './services/Apis'
import SingleOffert from './SingleOffert'
import FormMotsCles from './SearchForm/FormMotsCles'
import FormCommunes from './SearchForm/FormCommunes'
import FormDistance from './SearchForm/FormDistance'
import FormDepartement from './SearchForm/FormDepartement'
import FormRegions from './SearchForm/FormRegions'
import FormContrat from './SearchForm/FormContrat'
import RadioTri from './SearchForm/RadioTri'
import RadioTempsTravail from './SearchForm/RadioTempsTravail'
import LeafletMap from './services/LeafletMap'
import ScrollUpBtn from './services/ScrollUpButton'
import PaginationButtons from './services/PaginationButtons'

class Body extends Component {
  constructor () {
    super()
    this.state = {
      dataApi: [],
      tokenType: '',
      accessToken: '',
      codeInsee: '',
      allCommunesNames: '',
      allCommunesCode: '',
      range: '',
      motsCles: '',
      commune: '',
      distance: '0',
      contrat: '',
      tempsPlein: '',
      tri: '',
      departement: '',
      region: '',
      reset: '',
      showing: ''
    }
  }
  // Call Imput value and crates STATES
  callMotsClesValue (data) {
    this.setState({
      motsCles: data,
    })
  }
  callCommunesValue (data) {
    this.setState({
      commune: data,
      departement: '',
      region: ''
    })
  }
  callDistanceValue (data) {
    this.setState({
      distance: data,
    })
    this.fetchData()
  }
  callDepartementValue (data) {
    this.setState({
      departement: data,
      region: '',
      commune: ''
    })
  }
  callRegionValue (data) {
    this.setState({
      region: data,
      commune: '',
      departement: ''
    })
  }
  callContratsValue (data) {
    this.setState({
      contrat: data,
    })
  }
  callRadioTriValue (data) {
    this.setState({
      tri: data,
    })
    this.fetchData()
  }
  callRadioTempsTravailValue (data) {
    this.setState({
      tempsPlein: data,
    })
    this.fetchData()
  }
  callRangeValue (data) {
    this.setState({
      range: data,
    })
    this.fetchData()
    window.scrollTo({ top: 700, behavior: 'smooth' })
  }
  // Change code Insse for Arrondissement
  changeCodeForParisLyonMarseille (data) {
    // Paris
    if (this.state.commune === 'Paris') {
      this.setState({ codeInsee: '75101' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 1er Arrondissement') {
      this.setState({ codeInsee: '75101' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 2e Arrondissement') {
      this.setState({ codeInsee: '75102' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 3e Arrondissement') {
      this.setState({ codeInsee: '75103' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 4e Arrondissement') {
      this.setState({ codeInsee: '75104' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 5e Arrondissement') {
      this.setState({ codeInsee: '75105' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 6e Arrondissement') {
      this.setState({ codeInsee: '75106' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 7e Arrondissement') {
      this.setState({ codeInsee: '75107' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 8e Arrondissement') {
      this.setState({ codeInsee: '75108' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 9e Arrondissement') {
      this.setState({ codeInsee: '75109' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 10e Arrondissement') {
      this.setState({ codeInsee: '75110' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 11e Arrondissement') {
      this.setState({ codeInsee: '75111' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 12e Arrondissement') {
      this.setState({ codeInsee: '75112' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 13e Arrondissement') {
      this.setState({ codeInsee: '75113' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 14e Arrondissement') {
      this.setState({ codeInsee: '75114' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 15e Arrondissement') {
      this.setState({ codeInsee: '75115' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 16e Arrondissement') {
      this.setState({ codeInsee: '75116' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 17e Arrondissement') {
      this.setState({ codeInsee: '75117' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 18e Arrondissement') {
      this.setState({ codeInsee: '75118' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 19e Arrondissement') {
      this.setState({ codeInsee: '75119' })
      this.getDataApi()
    } else if (this.state.commune === 'Paris 20e Arrondissement') {
      this.setState({ codeInsee: '75120' })
      this.getDataApi()
      // Lyon
    } else if (this.state.commune === 'Lyon') {
      this.setState({ codeInsee: '69381' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 1er Arrondissement') {
      this.setState({ codeInsee: '69381' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 2e Arrondissement') {
      this.setState({ codeInsee: '69382' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 3e Arrondissement') {
      this.setState({ codeInsee: '69383' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 4e Arrondissement') {
      this.setState({ codeInsee: '69384' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 5e Arrondissement') {
      this.setState({ codeInsee: '69385' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 6e Arrondissement') {
      this.setState({ codeInsee: '69386' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 7e Arrondissement') {
      this.setState({ codeInsee: '69387' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 8e Arrondissement') {
      this.setState({ codeInsee: '69388' })
      this.getDataApi()
    } else if (this.state.commune === 'Lyon 9e Arrondissement') {
      this.setState({ codeInsee: '69389' })
      this.getDataApi()
      // Marseille
    } else if (this.state.commune === 'Marseille') {
      this.setState({ codeInsee: '13201' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 1er Arrondissement') {
      this.setState({ codeInsee: '13201' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 2e Arrondissement') {
      this.setState({ codeInsee: '13202' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 3e Arrondissement') {
      this.setState({ codeInsee: '13203' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 4e Arrondissement') {
      this.setState({ codeInsee: '13204' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 5e Arrondissement') {
      this.setState({ codeInsee: '13205' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 6e Arrondissement') {
      this.setState({ codeInsee: '13206' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 7e Arrondissement') {
      this.setState({ codeInsee: '13207' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 8e Arrondissement') {
      this.setState({ codeInsee: '13208' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 9e Arrondissement') {
      this.setState({ codeInsee: '13209' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 10e Arrondissement') {
      this.setState({ codeInsee: '13210' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 11e Arrondissement') {
      this.setState({ codeInsee: '13211' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 12e Arrondissement') {
      this.setState({ codeInsee: '13212' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 13e Arrondissement') {
      this.setState({ codeInsee: '13213' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 14e Arrondissement') {
      this.setState({ codeInsee: '13214' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 15e Arrondissement') {
      this.setState({ codeInsee: '13215' })
      this.getDataApi()
    } else if (this.state.commune === 'Marseille 16e Arrondissement') {
      this.setState({ codeInsee: '13216' })
      this.getDataApi()
    } else {
      this.setState({ codeInsee: data[0].code })
      this.getDataApi()
    }
  }
  // Take Data API and use TOKEN Fetched
  getDataApi () {
    fetch(`https://api.emploi-store.fr/partenaire/offresdemploi/v2/offres/search?distance=${ this.state.distance }&region=${ this.state.region }&departement=${ this.state.departement }&commune=${ this.state.codeInsee }&motsCles=${ this.state.motsCles }&typeContrat=${ this.state.contrat }&sort=${ this.state.tri }&tempsPlein=${ this.state.tempsPlein }&range=${ this.state.range } `, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `${ this.state.tokenType } ${ this.state.accessToken }`
      },
    })
      .then(res => res.json())
      .catch(err => (err))
      .then(data => {
      // Create State all data from Api
        this.setState({ dataApi: data.resultats })
      })
  }
  fetchData () {
    fetchToken() // Take TOKEN Api from server
      .then(data => {
      // Crate State tokenType and accessToken for access to Data Api
        this.setState({ tokenType: data.map(el => el.token_type) })
        this.setState({ accessToken: data.map(el => el.access_token) })

        if (this.state.commune !== '') {
          fetchInsee(this.state.commune) // Take Code INSEE de la commune
            .then(data => {
              this.changeCodeForParisLyonMarseille(data)
            })
        } else {
          fetchInsee(this.state.commune) // Take Code INSEE de la commune
            .then(data => {
              this.getDataApi()
            })
        }
      })
  }
  setResetStatus () {
    this.setState({ reset: 'clear' })
  }
  render () {
    const { showing } = this.state
    return (
      <div className="body">
        {/* Search Form */}
        <section className='formSection'>
          <div className='form'>
            <h2>Recherche emplois</h2>
            <div className='insideForm'>
              <form className='formSearch'>
                <div className='RegionInput'>
                  <FormRegions commune= {this.state.commune} departement= {this.state.departement} callbackFromParent={this.callRegionValue.bind(this)} reset= {this.state.reset} />
                </div>
                <p>OU</p>
                <div className='DepartementInput' >
                  <FormDepartement commune= {this.state.commune} region= {this.state.region} callbackFromParent={this.callDepartementValue.bind(this)} reset= {this.state.reset} />
                </div>
                <p>OU</p>
                <div className='CommunesAndDistanceImput'>
                  <div className='CommunesImput'>
                    <FormCommunes departement= {this.state.departement} region= {this.state.region} callbackFromParent={this.callCommunesValue.bind(this)} reset= {this.state.reset} />
                  </div>
                  <div className='DistanceImput'>
                    <FormDistance departement= {this.state.departement} region= {this.state.region} callbackFromParent={this.callDistanceValue.bind(this)} reset= {this.state.reset} />
                  </div>
                </div>
                <button onClick={this.setResetStatus.bind(this)} type="button" className="btn btn-danger btn-sm clearSelected" >Effacer sélectionné</button>
              </form>
              {/* Radios Buttons */}
              <div className='radioEtImputContainer'>
                <div className='MotsClesImput'>
                  <FormMotsCles callbackFromParent={this.callMotsClesValue.bind(this)}/>
                </div>
                <div className='ContratsImput'>
                  <FormContrat callbackFromParent={this.callContratsValue.bind(this)}/>
                </div>
                <div className='radioTri'>
                  <h3>Tri</h3>
                  <RadioTri callbackFromParent={this.callRadioTriValue.bind(this)}/>
                </div>
                <div className='radioTemsDeTravail'>
                  <h3>Temps de travail</h3>
                  <RadioTempsTravail callbackFromParent={this.callRadioTempsTravailValue.bind(this)}/>
                </div>
              </div>
              {/* End of Radios Buttons */}
            </div>
            {/* Search Button */}
            <button onClick={() => { this.fetchData(); this.setState({ showing: !showing }) }} type="button" className="btn btn-outline-secondary">Recherche</button>
          </div>
        </section>
        {/* End of Search Form */}
        <div className='resultsContainer'>
          <div className="result">
            <SingleOffert dataApi={this.state.dataApi}/>
            <div>
              { showing
                ? <PaginationButtons callbackFromParent={this.callRangeValue.bind(this)}/>
                : null
              }
            </div>
          </div>
          {/* Leaflet Map */}
          <div className='cardContainer'>
            <div className="Card">
              <LeafletMap departement={this.state.departement} region={this.state.region} codeInsee={this.state.codeInsee} dataApi={this.state.dataApi}/>
            </div>
          </div>
          {/* End Of Leaflet Map */}
        </div>
        <ScrollUpBtn/>
      </div>
    )
  }
}
export default Body
