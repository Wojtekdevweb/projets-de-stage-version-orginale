const fetch = require("node-fetch");
const stringify = require('qs-stringify');
const http = require('http');
const hostname = 'localhost';
const port = 8000;

//Get Token API
function myFunc() {
console.log('Hello, server restarts every 14 minutes');
fetch("https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire",{
    method:"POST",
    headers: {
             "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
             },
    body:   stringify({
            grant_type: "client_credentials",
            client_id: "PAR_projetdestage_7c57bfc8139ff5f0674703af91caeff635e54f85ec80c03df2ff06faeeac0dad",
            client_secret: "fea54e97617d89018e8cdc79d596eda5ed7534ce82327fe3ca3d2ad1be990dab",
            scope: "application_PAR_projetdestage_7c57bfc8139ff5f0674703af91caeff635e54f85ec80c03df2ff06faeeac0dad api_offresdemploiv2 o2dsoffre"
            })
})
.then((res) => res.json())
.catch((err) => console.log(err))
.then((data) => {
    let tokenJson = JSON.stringify([data])
        const server = http.createServer((req, res) => {
            res.statusCode = 200;
            res.setHeader('Access-Control-Allow-Origin','http://localhost:3000','Content-Type', 'text/plain');
            res.end(`${tokenJson}`);
        });
        server.listen(port, hostname, () => {
            console.log(`Server running at http://${hostname}:${port}/`);
        });
        setTimeout(function() {
            server.close();
        }, 836000); // server stop every 13.45 minutes
})
}
setImmediate(myFunc);
setInterval(myFunc, 840000); // server restarts every 14 minutes to get new Token

// for the application work, you must enter in the console log: set NODE_TLS_REJECT_UNAUTHORIZED=0
